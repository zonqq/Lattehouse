import "reflect-metadata";
import { createConnection, getMetadataArgsStorage } from "typeorm";
import { User } from "./entity/User";
let express = require("express")
let bodyParser = require('body-parser')
let con = createConnection()
let app = express()
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extend:false}))
app.post('/login', (req, res) => { 
   con.then( async connection => {
    let userRepo = connection.getRepository(User)
    let findUser = await userRepo.findOne({
        username: req.body.username,
        password: req.body.password
    })
    if(findUser === undefined){
        res.status(400).json({
            statusName:'wrong username or password'
        })
    }else{
        res.status(200).json({
            statusName:'login success'
        })
    }
   })
})
app.post('/Register', (req, res) => { 
    con.then( async connection => {
        let user = new User()
        user.username=req.body.username,
        user.password=req.body.password
    await connection.getRepository(User).save(user)
 })
})


app.listen(3001, () => console.log("server running"))

    
    

    