import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import { Actions } from "react-native-router-flux";

export default class Home extends React.Component {
  render() {
    return (
    <View>
      <View style={styles.box}>
          <Text style={styles.textProfile}>
            LATTEHOUSE
          </Text>
          <Text style={styles.mat}>
            Your Username
          </Text>
          <View style={styles.container1}>
          {/* <View style={styles.circle}> */}
            <Text style={styles.tex}>
            Project: Name
            </Text>
            {/* <View style={styles.circle}> */}
            <Text style={styles.tex}>
            24 June 2018 
            </Text>
            <Text style={styles.time}>
            Time left
            </Text>
            {/* <View style={styles.circle}> */}
            </View> 

            <View style={styles.container2}>
          {/* <View style={styles.circle}> */}
            <Text style={styles.tex}>
            Project: Name
            </Text>
            {/* <View style={styles.circle}> */}
            <Text style={styles.tex}>
            24 August 2018 
            </Text>
            <Text style={styles.time}>
            Time left
            </Text>
            {/* <View style={styles.circle}> */}
            </View> 
         </View>       
        </View>
      // </View>
      
    )
  }
}

const styles = StyleSheet.create({
  container1: {
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#36984C',
    margin: 17,
    width: 340,
    height: 210,
   },

   container2: {
    justifyContent: 'center',
    alignItems:'center',
    backgroundColor: '#ff6b6b',
    margin: 17,
    width: 340,
    height: 210,
   },
   mat: {
     fontSize: 30,
     marginLeft: 24,
     fontWeight: 'bold',
     textAlign: 'left',
     color: '#FFFFFF',
     alignItems: 'center'
   },
   box: { 
     backgroundColor: '#343434',
       width: 1000,
       height: 1000,
   },
   tex:{
     fontSize: 25,
     fontWeight: '300',
     margin: 10,
     textAlign: 'center',
     color: '#FFFFFF',
   },
   time:{
    fontSize: 20,
    margin: 10,
    textAlign: 'center',
    color: '#FFFFFF',
   },
   circle: {
    height: 500,
    width: 500,
    borderRadius: 70 ,
    borderWidth: 1,
   },

   textProfile: {
    fontSize: 16,
    margin: 24,
    fontWeight: 'bold',
    textAlign: 'left',
    color: '#FFFFFF',
    alignItems: 'center'
   }
});