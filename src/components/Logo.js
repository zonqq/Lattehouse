import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image
} from 'react-native';

export default class Logo extends Component {
    render() {
      return (
          <View style={styles.container}>
        <Image style = {{width: 150, height: 150}}
            source={require('../img/logo.jpg')}/>
            <Text style={styles.logoText}>LATTEHOUSE</Text>
            </View>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
      },

    logoText:{
        marginVertical: 15,
        fontSize: 30,
        fontWeight: '500'
        
    }
    });
  