import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableWithoutFeedback,
    TouchableOpacity
} from 'react-native';
import DismissKeyboard from 'dismissKeyboard';
import Logo from './Logo';
import FormSignup from './FormSignup';

import { Actions } from 'react-native-router-flux';


export default class Signup extends Component {

goback() {
    Actions.pop();
}

    render() {
      return (
        <TouchableWithoutFeedback onPress={()=>{DismissKeyboard()}}>
        <View style={styles.container}>
          <Logo/>
          <FormSignup type='Signup'/>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText}>Already have an account.</Text>
            <TouchableOpacity onPress={this.goback}><Text style={styles.signupButton}>Sign in</Text></TouchableOpacity>
          </View>
        </View>
        </TouchableWithoutFeedback>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
      },

     signupTextCont: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row'
     },
     
     signupText: {
         color:'#636e72',
        fontSize: 14
     },

     signupButton: {
        color: '#2d3436',
        fontSize:14,
        fontWeight: '500'

     },

     inputBox:{
        width: 300,
        backgroundColor:'#dfe6e9',
        borderRadius:20,
        paddingHorizontal:16,
        height: 50,
        fontSize: 16,
        color:'#2d3436',
        marginVertical: 10
    
    }
    });
  