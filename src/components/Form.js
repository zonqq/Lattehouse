import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    KeyboardAvoidingView
} from 'react-native';

import { Actions } from 'react-native-router-flux';


export default class Login extends Component {
    
    home() {
        Actions.home()
    }

    render() {
      return (
          
        // <View style={styles.container}>
        <KeyboardAvoidingView style={styles.container} behavior='padding' enable>
        <View>
          <TextInput style={styles.inputBox} 
            underlineColorAndroid='rgba(0,0,0,0)' 
            placeholder='Email'
            placeholderTextColor='#b2bec3'
            selectionColor='#ff9f43'
            keyboardType="email-address"
            onSubmitEditing={() => this.password.focus()}
          />
          <TextInput style={styles.inputBox} 
            underlineColorAndroid='rgba(0,0,0,0)' 
            placeholder='Password'
            secureTextEntry={true}
            placeholderTextColor='#b2bec3'
            selectionColor='#ff9f43'
            ref={(input) => this.password = input}
          />

          <TouchableOpacity onPress={this.home} style={styles.button}>
              <Text style={styles.buttonText}>{this.props.type}</Text>
          </TouchableOpacity>
          </View>
          </KeyboardAvoidingView>
        
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
      },

    inputBox:{
        width: 300,
        backgroundColor:'#dfe6e9',
        borderRadius:20,
        paddingHorizontal:16,
        height: 50,
        fontSize: 16,
        color:'#2d3436',
        marginVertical: 10
    
    },  
    
    button: {
        width:300,
        backgroundColor:'#fdcb6e',
        borderRadius:20,
        marginVertical: 10, 
        paddingVertical: 12
    },

    buttonText:{
        fontSize:16,
        fontWeight:'500',
        color:'#ffffff',
        textAlign: 'center'
    }

    });
  