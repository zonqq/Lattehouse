import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback
} from 'react-native';
import DismissKeyboard from 'dismissKeyboard';
import Logo from './Logo';
import Form from './Form';
import FormSignup from './FormSignup';
import { Actions } from 'react-native-router-flux';

export default class Login extends Component {

    signup() {
        Actions.signup()
    }

    render() {
      return (
        <TouchableWithoutFeedback onPress={()=>{DismissKeyboard()}}>
        <View style={styles.container}>
          <Logo/>
          <Form type="Login"/>
          <View style={styles.signupTextCont}>
            <Text style={styles.signupText}>Don't have an account yet?</Text>
            <TouchableOpacity onPress={this.signup}><Text style={styles.signupButton}>Signup</Text></TouchableOpacity>
          </View>
        </View>
        </TouchableWithoutFeedback>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
      },

     signupTextCont: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 16,
        flexDirection: 'row'
     },
     
     signupText: {
         color:'#636e72',
        fontSize: 14
     },

     signupButton: {
        color: '#2d3436',
        fontSize:14,
        fontWeight: '500'

     }
    });
  