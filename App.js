import React, { Component} from 'react';
import { StyleSheet, View } from 'react-native';
import {Router, Stack, Scene} from 'react-native-router-flux'

import Signup from './src/components/Signup';
import Login from './src/components/Login';
import Home from './src/components/Home';


export default class App extends Component {
  render() {
    return (
      <Router>
                <Stack key='root' hideNavBar={true}>
                    <Scene key="login" component={Login} title="Login" initial={true}/>
                    <Scene key="signup" component={Signup} title="Signup"/>
                    <Scene key="home" component={Home} title="Home"/>
                </Stack>
            </Router>
      // <View style={styles.container}>
        
      //   <Login/>
      // </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
